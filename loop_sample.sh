#!/bin/bash

main_cmd='ipmitool raw 0x0a 0x44 0 0 0xc0 0 0 0 0 0xeb 0x12 0x00'

execute_cmd() {
  local s result='FAIL'
  local run_cmd="${main_cmd} ${cmd}"
  local excepted_value=$(echo "${cmd}" | sed "s/0x//g;s/ //g")
  echo
  ipmitool sel clear
  echo "${run_cmd}"
  for ((s=0; s<30; s++)); do
    ${run_cmd}
    sleep 2
    sel="$(ipmitool sel elist | grep '0012eb')"
    [[ -z "${sel}" ]] && continue
    echo "${sel}"
    [[ $(echo "${sel}" | awk '{print $NF}') != ${excepted_value,,} ]] && exit 1
    break
  done
}

get_random() {
  local j random_value hex value=''
  for ((j=0; j<6; j++)); do
    if [[ $j -eq 2 ]]; then
      hex=$(echo "obase=16;$i" | bc)
    else 
      hex=$(echo "obase=16;$(($RANDOM % 255))" | bc)
    fi
    [[ ${#hex} -eq 1 ]] && hex="0${hex}"
    value+="0x${hex,,} "
  done
  echo "${value}"
}

run() {
  local i cmd

  for ((i=0; i<256; i++)); do
    cmd=$(get_random)
    execute_cmd
  done
}

main() {
  run
}

log_name="$1"
[[ -z ${log_name} ]] && log_name='loop.log'

main | tee ${log_name}

