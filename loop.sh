#!/bin/bash

main_cmd='ipmitool raw 0x0a 0x44 0 0 0xc0 0 0 0 0 0xeb 0x12 0x00'

execute_cmd() {
  local s result='FAIL'
  local run_cmd="${cmd} 0x${hex}"
  local excepted_value=$(echo "${run_cmd}" | sed "s/${main_cmd}//;s/ 0x//g")
  echo
  ipmitool sel clear
  echo "${run_cmd}"
  for ((s=0; s<30; s++)); do
    ${run_cmd}
    sleep 2
    sel="$(ipmitool sel elist | grep '0012eb')"
    [[ -z "${sel}" ]] && continue
    echo "${sel}"
    [[ $(echo "${sel}" | awk '{print $NF}') != ${excepted_value,,} ]] && exit 1
    break
  done
}

run() {
  local layer="$1"
  local cmd="$2"

  local i hex
  for ((i=0; i<256; i++)); do
    hex=$(echo "obase=16;$i" | bc)
    [[ ${#hex} -eq 1 ]] && hex="0${hex}"
    if [[ ${layer} == 6 ]]; then
      execute_cmd
    else
      run "$((layer+1))" "${cmd} 0x${hex}"
      layer=$?
    fi
  done
  return $((layer-1))
}

main() {
  run "1" "${main_cmd}"
}

main

